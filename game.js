var level=1;
var userClickedPattern = [];
var gamePattern = [];
var buttonColours = ["red", "blue", "green", "yellow"];
var started=false;
$(".btn").click(function () {
    var userChosenColour = $(this).attr("id");
   
    userClickedPattern.push(userChosenColour);
    playSound(userChosenColour);
    animatePress(userChosenColour);
   
   checkAnswer(userClickedPattern.length-1);
   
    
    
});
$(document).keypress(function () {
    if(!started)
    {
         sequence();
        started=true;
    }
    
});



function sequence() {

    userClickedPattern=[];      
        $("#level-title").text("level "+level);
        level=level+1;
        var randomNumber = Math.floor(Math.random() * 4);

        var randomChosenColour = buttonColours[randomNumber];
        gamePattern.push(randomChosenColour);

        $("#" + randomChosenColour).fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100);
        playSound(randomChosenColour);
       
    }
   



function playSound(name) {

    var audio = new Audio("sounds/" + name + ".mp3");
    audio.play();
}
function animatePress(currentColour) {
    $("#" + currentColour).addClass("pressed");


    setTimeout(function () {
        $("#" + currentColour).removeClass("pressed");
    }, 100);
}
function checkAnswer(currentLevel)
{
    if(gamePattern[currentLevel] === userClickedPattern[currentLevel])
    {
        console.log("right");
        if(userClickedPattern.length === gamePattern.length)
        {
            setTimeout(function () {
            sequence();
            }, 1000);
        }
  
    }
    else{
        console.log("wrong");
        playSound("wrong");
        $("body").addClass("game-over");

        setTimeout(function ()
        {
            $("body").removeClass("game-over");
        },200);
        $("#level-title").text("Game over,Press Any Key to Restart");
        startOver();
    }

}
function startOver()
{
    level=1;
    gamePattern=[];
    started=false;
}